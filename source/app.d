// Copyright 2019 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// -----------------------------------------------------------------------------
// [Translated from] File: robots_main.cc
// -----------------------------------------------------------------------------
//
// Simple binary to assess whether a URL is accessible to a user-agent according
// to records found in a local robots.txt file, based on Google's robots.txt
// parsing and matching algorithms.
// Usage:
//     robots_main <local_path_to_robotstxt> <user_agent> <url>
// Arguments:
// local_path_to_robotstxt: local path to a file containing robots.txt records.
//   For example: /home/users/username/robots.txt
// user_agent: a token to be matched against records in the robots.txt.
//   For example: Googlebot
// url: a url to be matched against records in the robots.txt. The URL must be
// %-encoded according to RFC3986.
//   For example: https://example.com/accessible/url.html
// Returns: Prints a sentence with verdict about whether 'user_agent' is allowed
// to access 'url' based on records in 'local_path_to_robotstxt'.
//

import std.algorithm;
import std.array;
import std.file;
import io = std.stdio;

import unrobotstxt;

void showHelp(const(string)[] args)
{
	io.stderr.writeln("Shows whether the given user_agent and URI combination is allowed or disallowed by the given robots.txt file. \n");
	io.stderr.writeln("Usage: ");
	io.stderr.writef(" %s <robots.txt filename> <user_agent> <URI>\n", args[0]);
	io.stderr.writeln("The URI must be %-encoded according to RFC3986.");
	io.stderr.writeln("Example: ");
	io.stderr.writef(" %s robots.txt FooBot http://example.com/foo\n", args[0]);
}

int main(string[] args)
{
	auto filename = args.length >= 2 ? args[1] : "";
	if (filename.among("-h", "-help", "--help"))
	{
		showHelp(args);
		return 0;
	}
	if (args.length != 4)
	{
		io.stderr.writeln("Invalid amount of arguments. Showing help.");
		showHelp(args);
		return 1;
	}

	string robots_content;
	try
	{
		robots_content = cast(string)read(filename);
	}
	catch (FileException e)
	{
		io.stderr.writef("failed to read file\"%s\"\n", filename);
		return 1;
	}

	auto user_agent = args[2];
	auto user_agents = [user_agent];
	auto matcher = new RobotsMatcher;
	auto url = args[3];
	auto allowed = matcher.AllowedByRobots(robots_content, user_agents, url);

	io.writef("user-agent '%s' with URI '%s': %s\n", user_agent, url, allowed ? "ALLOWED" : "DISALLOWED");
	if (robots_content.empty)
	{
		io.writeln("notice: robots file is empty so all user-agents are allowed");
	}
	return 0;
}
